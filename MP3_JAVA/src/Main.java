/*
* Creado por Eduardo Hernández Almanza
* Multimedia
* 19-09-18
* Referencias para la libreria.
* https://github.com/mpatric/mp3agic
*
*
*
* */



import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;
import static java.lang.Math.toIntExact;
import java.io.RandomAccessFile;

public class Main {

    public static void main(String[] args)throws Exception {

        System.out.println("Hello World!");

        //Mp3File mp3file = new Mp3File("Songs/04 You're My Best Friend.mp3");    //Canción con Portada
        Mp3File mp3file = new Mp3File("Songs/Artic.mp3");               //Canción sin Portada
        System.out.println("Length of this mp3 is: " + mp3file.getLengthInSeconds() + " seconds");   //Datos generales de la canción
        System.out.println("Bitrate: " + mp3file.getBitrate() + " kbps " + (mp3file.isVbr() ? "(VBR)" : "(CBR)"));
        System.out.println("Sample rate: " + mp3file.getSampleRate() + " Hz");
        System.out.println("Has ID3v1 tag?: " + (mp3file.hasId3v1Tag() ? "YES" : "NO"));
        System.out.println("Has ID3v2 tag?: " + (mp3file.hasId3v2Tag() ? "YES" : "NO"));
        System.out.println("Has custom tag?: " + (mp3file.hasCustomTag() ? "YES" : "NO"));

        if (mp3file.hasId3v2Tag()) {
            ID3v2 id3v2Tag = mp3file.getId3v2Tag();
            byte[] albumImageData = id3v2Tag.getAlbumImage();
            if (albumImageData != null) {
                System.out.println("Have album image data, length: " + albumImageData.length + " bytes");
                System.out.println("Album image mime type: " + id3v2Tag.getAlbumImageMimeType());
                String mimeType = id3v2Tag.getAlbumImageMimeType();
                // Write image to file - can determine appropriate file extension from the mime type
                RandomAccessFile file = new RandomAccessFile("album-artwork.jpg", "rw");
                file.write(albumImageData);
                file.close();

            }
            else{
                RandomAccessFile cover = new RandomAccessFile("Covers/newcover.jpg","r");
                byte[] coverdata = new byte[toIntExact(cover.length())];  // SE crea un arreglo de bytes del tamaño
                cover.read(coverdata);
                cover.close();
                id3v2Tag.setAlbumImage(coverdata, "image/jpeg");
                mp3file.save("SongwithCover.mp3");
            }


        }





    }

}
